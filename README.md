PGF DB is an Ubuntu 14.04 image with a PostgreSQL 9.3 RDBMS for supporting 
the [Policy Governance Function (PGF)](https://gitlab.com/ontic/ericsson-pgf-server)
to store all the current degradation situations. 

The image is available in [Docker Hub](https://hub.docker.com/r/onticericssonspain/pgf-db/) 
and can be executed in a [Docker Compose environment](https://gitlab.com/ontic/ericsson-uc3-demo2)
