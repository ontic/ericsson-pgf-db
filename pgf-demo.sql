\c pgf
INSERT INTO degradation.session VALUES ('4926d687-1729-4ec0-b23f-1d538fa06ac6', '7fe87528-c213-11e6-a4a6-cec0c932ce01', '2', '1', '1481022000000', '1481022000000', '1481022000000', '1481025000000', 0.70, '');
INSERT INTO degradation.session_affects_customer_segment VALUES ('4926d687-1729-4ec0-b23f-1d538fa06ac6', 1, 28);
INSERT INTO degradation.session_affects_customer_segment VALUES ('4926d687-1729-4ec0-b23f-1d538fa06ac6', 2, 32);
INSERT INTO degradation.session_affects_customer_segment VALUES ('4926d687-1729-4ec0-b23f-1d538fa06ac6', 3, 10);
INSERT INTO degradation.session_affects_customer_segment VALUES ('4926d687-1729-4ec0-b23f-1d538fa06ac6', 4, 20);
INSERT INTO degradation.session_affects_customer_segment VALUES ('4926d687-1729-4ec0-b23f-1d538fa06ac6', 5, 10);
INSERT INTO degradation.session_refers_to_kpi VALUES ('4926d687-1729-4ec0-b23f-1d538fa06ac6', 'video_qoe', 92);

INSERT INTO degradation.session VALUES ('6d8a91c0-445b-448d-8576-f89bc5e7ee98', '7fe87640-c213-11e6-a4a6-cec0c932ce01', '4', '1', '1481026000000', '1481026000000', '1481026000000', '1481028000000', 0.65, '');
INSERT INTO degradation.session_affects_customer_segment VALUES ('6d8a91c0-445b-448d-8576-f89bc5e7ee98', 1, 28);
INSERT INTO degradation.session_affects_customer_segment VALUES ('6d8a91c0-445b-448d-8576-f89bc5e7ee98', 2, 32);
INSERT INTO degradation.session_affects_customer_segment VALUES ('6d8a91c0-445b-448d-8576-f89bc5e7ee98', 3, 10);
INSERT INTO degradation.session_affects_customer_segment VALUES ('6d8a91c0-445b-448d-8576-f89bc5e7ee98', 4, 20);
INSERT INTO degradation.session_affects_customer_segment VALUES ('6d8a91c0-445b-448d-8576-f89bc5e7ee98', 5, 10);
INSERT INTO degradation.session_refers_to_kpi VALUES ('6d8a91c0-445b-448d-8576-f89bc5e7ee98', 'video_qoe', 95);

INSERT INTO degradation.session VALUES ('9b3764d1-abbb-4c49-a2a6-a90473f842db', '7fe87726-c213-11e6-a4a6-cec0c932ce01', '5', '1', '1481056000000', '1481056000000', '1481056000000', '14810598000000', 0.20, '');
INSERT INTO degradation.session_affects_customer_segment VALUES ('9b3764d1-abbb-4c49-a2a6-a90473f842db', 1, 28);
INSERT INTO degradation.session_affects_customer_segment VALUES ('9b3764d1-abbb-4c49-a2a6-a90473f842db', 2, 32);
INSERT INTO degradation.session_affects_customer_segment VALUES ('9b3764d1-abbb-4c49-a2a6-a90473f842db', 3, 10);
INSERT INTO degradation.session_affects_customer_segment VALUES ('9b3764d1-abbb-4c49-a2a6-a90473f842db', 4, 20);
INSERT INTO degradation.session_affects_customer_segment VALUES ('9b3764d1-abbb-4c49-a2a6-a90473f842db', 5, 10);
INSERT INTO degradation.session_refers_to_kpi VALUES ('9b3764d1-abbb-4c49-a2a6-a90473f842db', 'video_qoe', 91);

INSERT INTO degradation.session VALUES ('4cf87ec2-2585-4052-afdd-5e60259bfa82', '7fe877f8-c213-11e6-a4a6-cec0c932ce01', '1', '1', '1481066000000', '1481066000000', '1481066000000', '1481069000000', 0.98, '');
INSERT INTO degradation.session_affects_customer_segment VALUES ('4cf87ec2-2585-4052-afdd-5e60259bfa82', 1, 28);
INSERT INTO degradation.session_affects_customer_segment VALUES ('4cf87ec2-2585-4052-afdd-5e60259bfa82', 2, 32);
INSERT INTO degradation.session_affects_customer_segment VALUES ('4cf87ec2-2585-4052-afdd-5e60259bfa82', 3, 10);
INSERT INTO degradation.session_affects_customer_segment VALUES ('4cf87ec2-2585-4052-afdd-5e60259bfa82', 4, 20);
INSERT INTO degradation.session_affects_customer_segment VALUES ('4cf87ec2-2585-4052-afdd-5e60259bfa82', 5, 10);
INSERT INTO degradation.session_refers_to_kpi VALUES ('4cf87ec2-2585-4052-afdd-5e60259bfa82', 'video_qoe', 50);

INSERT INTO degradation.session VALUES ('95164524-0235-4768-ab3e-a278ab2d0c75', '7fe8726c-c213-11e6-a4a6-cec0c932ce01', '0', '-1', '1480759200000', '1480759200000', '1480759200000', '1480761000000', 0.78, '');
INSERT INTO degradation.session_affects_customer_segment VALUES ('95164524-0235-4768-ab3e-a278ab2d0c75', 1, 28);
INSERT INTO degradation.session_affects_customer_segment VALUES ('95164524-0235-4768-ab3e-a278ab2d0c75', 2, 32);
INSERT INTO degradation.session_affects_customer_segment VALUES ('95164524-0235-4768-ab3e-a278ab2d0c75', 3, 10);
INSERT INTO degradation.session_affects_customer_segment VALUES ('95164524-0235-4768-ab3e-a278ab2d0c75', 4, 20);
INSERT INTO degradation.session_affects_customer_segment VALUES ('95164524-0235-4768-ab3e-a278ab2d0c75', 5, 10);
INSERT INTO degradation.session_refers_to_kpi VALUES ('95164524-0235-4768-ab3e-a278ab2d0c75', 'video_qoe', 40);
