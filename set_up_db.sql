CREATE USER pgf WITH PASSWORD 'pgf';
CREATE DATABASE pgf;
ALTER DATABASE pgf OWNER TO pgf;
ALTER USER pgf WITH SUPERUSER;
\c pgf;

DROP SCHEMA IF EXISTS degradation CASCADE;
CREATE SCHEMA degradation;
SET search_path TO degradation,public;

CREATE TABLE degradation.report (
	id VARCHAR primary key, -- PRIMARY KEY   -uppdated v2 from serial to VARCHAR
	session_id VARCHAR, --FK done     --updated v2. from situation_id to session_id, from INT to VARCHAR
	location_id INT, --FK done
	report_timestamp VARCHAR NOT NULL,
	start_time VARCHAR NOT NULL,
	end_time VARCHAR NOT NULL,
	confidence DECIMAL NOT NULL,
	remarks TEXT
);

CREATE UNIQUE INDEX degradation_report_id
	ON degradation.report (id);
	
CREATE TABLE degradation.session(    -- updated on v2. this table is now called seesion instead of situation
	id VARCHAR primary key,    -- PRIMARY KEY
	latest_report_id VARCHAR, --FK done --v2 udated from int to VARCHAR
	location_id INT, --FK  done  --v2 from bigint to int
	mitigation_plan_id INT,
	creation_time VARCHAR NOT NULL,
	last_updated VARCHAR NOT NULL,
	start_time VARCHAR NOT NULL,
	end_time VARCHAR NOT NULL,
	confidence DECIMAL NOT NULL,
	remarks TEXT
	
);
	
CREATE UNIQUE INDEX degradation_session_id   --updated v2 from situation to session
	ON degradation.session (id);

CREATE TABLE degradation.mitigation_plan (
	id SERIAL primary key,    -- PRIMARY KEY
	name VARCHAR NOT NULL,
	priority INT NOT NULL
);

CREATE TABLE degradation.report_affects_customer_segment(
	report_id VARCHAR, -- pk / fk done   --v2 from int to VARCHAR
	customer_segment_id INT, -- pk/fk done --v2 from int to VARCHAR
	share real NOT NULL,
	PRIMARY KEY (report_id, customer_segment_id)

);

CREATE TABLE degradation.session_affects_customer_segment(   --v2 rename from situation to session
	session_id VARCHAR, -- pk / fk done     -- from situation_id to session_id   -- from int to char
	customer_segment_id VARCHAR, --pk/fk done --v2 from customer_segment_id to customer_segment_name (new PK)
	share real NOT NULL,
	PRIMARY KEY (session_id, customer_segment_id)   -- v2 from situation_it to session_id

);

CREATE TABLE degradation.mitigation_plan_is_made_of_network_policy(
        id serial NOT NULL primary key, -- pk
	mitigation_plan_id int, -- fk done
	network_policy_id int, -- fk done
        customer_segment_id int
);

CREATE TABLE degradation.report_refers_to_kpi(
	degradation_report_id VARCHAR, --pk/fk done
	kpi_name VARCHAR, --pk/fk done --v2 it changes, from kpi_id to kpi_name
	value INTEGER NOT NULL,
	PRIMARY KEY (degradation_report_id, kpi_name)
);

CREATE TABLE degradation.session_refers_to_kpi(  --v2 from situation to session
	degradation_session_id VARCHAR, --pk/fk done
	kpi_name VARCHAR, -- pk/fk done
	value INTEGER NOT NULL,
	PRIMARY KEY (degradation_session_id, kpi_name)
);

CREATE TABLE degradation.mitigation_order(
	session_id VARCHAR primary key,
	order_id VARCHAR,
	order_timestamp VARCHAR NOT NULL,
	status VARCHAR
);

DROP SCHEMA IF EXISTS technologies CASCADE;
CREATE SCHEMA technologies;
SET search_path TO technologies,public;

CREATE TABLE technologies.location (
	id INT primary key,     -- v2 from serial to INT because the location identifiers will be cell_ids
	technology character varying,
	lat character varying,
	lon character varying,
	city character varying,
	cell_id character varying,
	remarks text

);

CREATE TABLE technologies.kpi(
	-- v2 remove this field- kpis are identified by the name - id serial primary key,
	name character varying primary key, --new primary key
	service character varying NOT NULL, -- TODO it is needed to add a service table to describe the service
	warning_threshold real,
	critical_threshold real,
	expected_best_value  real,
	expected_worst_value real,
	remarks text

);

DROP SCHEMA IF EXISTS subscriber CASCADE;
CREATE SCHEMA subscriber;
SET search_path TO subscriber,public;

CREATE TABLE subscriber.subscriber (
	id SERIAL primary key, -- PRIMARY KEY
	customer_segment_name varchar, --FK done
	msisdn character varying,
	imei character varying,
	remarks text
);

CREATE UNIQUE INDEX subscriber_id
	ON subscriber.subscriber(id);
		
CREATE TABLE subscriber.customer_segment(
	id SERIAL primary key,    -- PRIMARY KEY
	name VARCHAR, 
	remarks text
);
	
CREATE UNIQUE INDEX subscriber_group_id
	ON subscriber.customer_segment (name);

DROP SCHEMA IF EXISTS policies CASCADE;
CREATE SCHEMA policies;
SET search_path TO policies,public;

CREATE TABLE policies.network_policy (
	id serial NOT NULL primary key,
	name character varying NOT NULL,
	policy_type character varying NOT NULL,
	params character varying,
        image_name character varying,
	remarks text
);

CREATE UNIQUE INDEX policies_network_policy_id
	ON policies.network_policy(id);

DROP SCHEMA IF EXISTS configuration CASCADE;
CREATE SCHEMA configuration;
SET search_path TO configuration,public;
CREATE TABLE configuration.configuration (
	key text primary key, -- PRIMARY KEY
	value text	
);


insert into technologies.KPI VALUES ('video_freeze_rate','Video',50,70,0,100,' ');
insert into technologies.KPI VALUES ('video_accessibility','Video',90,80,100,0,' ');
insert into technologies.KPI VALUES ('video_qoe','Video',75,60,100,0,' ');
insert into technologies.KPI VALUES ('file_transfer_accessibility','File Transfer',90,80,100,0,' ');
insert into technologies.KPI VALUES ('web_browsing_accessibility','Web Browsing',90,80,100,0,' ');

insert into technologies.location VALUES (0,' ','40.468393','-3.680223','Madrid','132909340',' ');
insert into technologies.location VALUES (1,' ','41.388995','2.172114','Barcelona','132909543',' ');
insert into technologies.location VALUES (2,' ','43.365806','-5.849344','Oviedo','132909976',' ');
insert into technologies.location VALUES (3,' ','42.597047','-5.574207','León','132909596',' ');
insert into technologies.location VALUES (4,' ','36.721364','-4.412173','Málaga','132901849',' ');
insert into technologies.location VALUES (5,' ','40.967392','-5.671787','Salamanca','132906098',' ');
insert into technologies.location VALUES (6,' ','37.397876','-5.987331','Sevilla','132904442',' ');

insert into subscriber.customer_segment VALUES (DEFAULT,'gold','');
insert into subscriber.customer_segment VALUES (DEFAULT,'silver corporate','');
insert into subscriber.customer_segment VALUES (DEFAULT,'silver domestic','');
insert into subscriber.customer_segment VALUES (DEFAULT,'bronze corporate','');
insert into subscriber.customer_segment VALUES (DEFAULT,'bronze young','');

INSERT INTO policies.network_policy VALUES (DEFAULT, 'wifi', 'Wifi Offloading', '', 'wifi', '');
INSERT INTO policies.network_policy VALUES (DEFAULT, 'to3g', 'Switch to 3G', '', '4Gto3G', '');
INSERT INTO policies.network_policy VALUES (DEFAULT, 'to2g', 'Switch to 2G', '', '3Gto2G', '');
INSERT INTO policies.network_policy VALUES (DEFAULT, 'traffic_gating', 'Traffic Gating', 'video', 'traffic_gating_video', '');
INSERT INTO policies.network_policy VALUES (DEFAULT, 'traffic_gating', 'Traffic Gating', 'file_transfer', 'traffic_gating_file_transfer', '');
INSERT INTO policies.network_policy VALUES (DEFAULT, 'traffic_gating', 'Traffic Gating', 'web_browsing', 'traffic_gating_web_browsing', '');
INSERT INTO policies.network_policy VALUES (DEFAULT, 'traffic_gating', 'Traffic Gating', 'video|file_transfer', 'traffic_gating_video-file_transfer', '');
INSERT INTO policies.network_policy VALUES (DEFAULT, 'traffic_gating', 'Traffic Gating', 'video|web_browsing', 'traffic_gating_video-web_browsing', '');
INSERT INTO policies.network_policy VALUES (DEFAULT, 'traffic_gating', 'Traffic Gating', 'video|file_transfer|web_browsing', 'traffic_gating_video-file_transfer-web_browsing', '');
INSERT INTO policies.network_policy VALUES (DEFAULT, 'bandwidth_throttling', 'Bandwidth Throttling', '64', '64Kbps', '');
INSERT INTO policies.network_policy VALUES (DEFAULT, 'bandwidth_throttling', 'Bandwidth Throttling', '1000', '1Mbps', '');
INSERT INTO policies.network_policy VALUES (DEFAULT, 'bandwidth_throttling', 'Bandwidth Throttling', '3000', '3Mbps', '');
INSERT INTO policies.network_policy VALUES (DEFAULT, 'video_acceleration', 'Video Acceleration', '', 'video_acceleration', '');

insert into configuration.configuration VALUES ('sim_ip','localhost');
insert into configuration.configuration VALUES ('sim_port','8008');
insert into configuration.configuration VALUES ('sim_path','sim_recommend/recommendation/');
insert into configuration.configuration VALUES ('sim_enabled','false');
insert into configuration.configuration VALUES ('policies','bandwidth_throttling_3000,bandwidth_throttling_1000,bandwidth_throttling_64');
insert into configuration.configuration VALUES ('kpis','video_freeze_rate,video_accessibility');
insert into configuration.configuration VALUES ('skin','ontic');
insert into configuration.configuration VALUES ('enf_point_ip','proxy');
insert into configuration.configuration VALUES ('enf_point_port','8000');
insert into configuration.configuration VALUES ('mit_mode','predefined');
insert into configuration.configuration VALUES ('predef_plan_id','1');
insert into configuration.configuration VALUES ('google_maps_api_key','AIzaSyA-rNZnfChhCV3ttCcM_-pMzKIOIwu115A');
insert into configuration.configuration VALUES ('debug','false');
insert into configuration.configuration VALUES ('timestamp_check','false');
-- Insert one generic plan
INSERT INTO degradation.mitigation_plan VALUES (DEFAULT, 'Bandwidth Throttling Plan', 0);
INSERT INTO degradation.mitigation_plan_is_made_of_network_policy VALUES (DEFAULT, 1, 12, 1);
INSERT INTO degradation.mitigation_plan_is_made_of_network_policy VALUES (DEFAULT, 1, 11, 2);
INSERT INTO degradation.mitigation_plan_is_made_of_network_policy VALUES (DEFAULT, 1, 11, 3);
INSERT INTO degradation.mitigation_plan_is_made_of_network_policy VALUES (DEFAULT, 1, 10, 4);
INSERT INTO degradation.mitigation_plan_is_made_of_network_policy VALUES (DEFAULT, 1, 10, 5);

